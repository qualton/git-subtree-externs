git-subtree-externs
===================

git-subtree-externs is a python script to ease the use of subtrees as a means for managing "vendor" git repositories as a subdirectory of your main project (similar to how you may have used svn:externs)

*git-subtree* is a git command written by Avery Pennarun (github.com/apenwarr) which provides a great alternative to git submodules.  It is available from the official git source repo (github.com/git/git) under contrib/subtree -- it has also been included in this repo as a subtree using this git-subtree-externs script for convenience and example.  *git-subtree* is a prerequisite for git-subtree-externs.


FAQ
---
- *git-subtree* already does a great job of abstracting git plumbing (like read-tree) so why would I need this git-subtree-externs script?

        In most use cases you probably don't!

        It was created for these 2 use cases:

        1. Adding Custom Actions to SourceTree for those who prefer it over command-line (including myself)
        2. Simplifying the more arduous scenario where you only want a subdirectory of the vendor repo in your repo (and you want to be able to contribute local modifications back)

- What is SourceTree?

        SourceTree is a GUI for git made by the wonderful people at Atlassian (makers of JIRA, Confluence, and hosts of bitbucket.org).  You can get it here:  http://www.sourcetreeapp.com/

- If this is a git repo for a git script, why on earth didn't you do the git thing and fork *git-subtree*?

        Because bash scripting isn't my thing and at time of creation it's much more important to get things done than to deal with more hinderances.  Hopefully this script becomes obsolete because someone updates features of git-subtree to handle the 2nd use case, and SourceTree implements a workflow for subtrees eliminating the 1st use case.  I wouldn't hold my breath on *someone* actually _fixing_ git... but I have much more faith in the people who contrib who don't think git is all that.

        Wait, did you just imply that git was broken?  Yes!

        - "If the user can’t use it, it doesn’t work." -Susan Dray

Basic Usage
-----------
(don't forget to ``chmod +x`` the .sh and .py files)

**From the command line:**

    $ ``cd <your git repo root>``

    See usage from
    $ ``git_subtree_externs.py --help``

**From SourceTree:**
  **Setup**
    
    SourceTree (menu) > Preferences > Custom Actions (tab) > Add

    - Menu caption = update subtree
    - Show Full Output = checked
    - Script to run = <path to>/sourcetree_subtree.sh
    - parameters = --update --vendorGuessFromSubtree=$FILE

    Repeat for menu "contribute subtree" with params --contribute  --vendorGuessFromSubtree=$FILE
    
    Repeat for menu "fetch subtree" with params --fetch  --vendorGuessFromSubtree=$FILE

  **Usage**

    With a development branch checked-out, select Working Copy > Show All

    Right-click a file under a subtree > Custom Actions > *action*

The --define and --implant one-time actions must be done from the command-line


License
-------
Copyright (c) 2013, Kevin Walton.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.