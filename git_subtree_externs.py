#! /usr/bin/python

"""
Copyright (c) 2013, Kevin Walton.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
__author__ = 'kwalton'


import os
import sys
import time
import optparse
import pprint

import ConfigParser
import subprocess

myGit = None


def runCmd( cmd ):
    try:
        print "RUN: "+cmd
        ret = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        print e.output
        raise Exception('running command failed: '+ e.cmd)

    return ret


def getRepoBasename( repo ):
    base = 'unk'
    if repo[-4:] == '.git':
        base = repo[repo.rfind('/')+1:-4]
    else:
        base = repo[repo.rfind(':')+1:]
    return base

def getSubtreeBranchName( repo ):
    return "subtree/" + getRepoBasename(repo)

def getVendorBranchName( repo ):
    return "vendor/" + getRepoBasename(repo)



class TreeConfig():

    def __init__(self, file='.gittrees'):
        self.config = ConfigParser.ConfigParser()
        self.filename = file
        self.config.read(file)

    @staticmethod
    def sectionId(name):
        # [subtree "project"]
        return 'subtree "' + name + '"'

    @staticmethod
    def sectionIdToName(id):
        # subtree "project"
        return id[9:-1]

    def vendorDefined(self, id):
        return self.config.has_section( TreeConfig.sectionId(id) )

    def getSubtreeCfg(self, id):
        params = self.config.items( TreeConfig.sectionId(id) )
        return dict(params)

    def defineSubtree(self, vendor, vendorRepo, **kwargs ):
        """
        [subtree "project"]
        url = git://bitbucket.org/user/project.git
        path = project
        branch = master
        """
        cfg = self.config
        sect = TreeConfig.sectionId(vendor)
        cfg.add_section(sect)
        cfg.set(sect, 'url', '.')   # we point to our own local tracking branch, we keep remote url in other metadata
        path = vendor if not kwargs.get('destDir') else kwargs.get('destDir')
        cfg.set(sect, 'path', path ) # or getRepoBasename(vendorRepo)
        cfg.set(sect, 'branch', getSubtreeBranchName(vendorRepo) )

        # other metadata for our use that git-subtree doesn't actually use
        cfg.set(sect, '_url', vendorRepo)
        cfg.set(sect, '_branch', kwargs.get( 'vendorBranch', 'master') )
        cfg.set(sect, '_subdir', kwargs.get( 'vendorDir', None) )

        with open(self.filename, 'wb') as configfile:
            self.config.write(configfile)

        runCmd("git add "+ self.filename)   # .gittrees changed, add to index

        return self.getSubtreeCfg(vendor)


    def guessVendorFromPath(self, path):
        sections = self.config.sections()
        for s in sections:
            name = TreeConfig.sectionIdToName(s)
            cfg = self.getSubtreeCfg( name )
            if path.startswith( cfg.get('path')+'/' ):
                return name


class GitConfig():
    useGit = True   # use git commands... otherwise read config -- read/write config file have file locking issues?

    def __init__(self, file='.git/config'):
        if file is None:
            self.useGit = True

        if self.useGit:
            pass
        else:
            self.config = ConfigParser.ConfigParser()
            self.config.read(file)

    def getRemoteByURI(self, uri):
        if self.useGit:
            output = runCmd("git remote -v")    # origin	git://bitbucket.org/user/project.git (fetch)
            outlines = output.splitlines()
            remotes = map( (lambda l: (lambda w=l.split(): [w[1],w[0]])()), outlines )
            remotesByURI = dict(remotes)
            return remotesByURI.get( uri )
        else:
            raise Exception('not implemented')

    def hasBranch(self, branch):
        if self.useGit:
            output = runCmd("git branch -l")
            outlines = output.splitlines()
            branches = [b[2:] for b in outlines]
            return branch in branches
        else:
            raise Exception('not implemented')

    def branchIsTracking(self, branch, remote):
        pass

    def addRemote(self, name, url):
        cmd = "git remote add "+name+" "+url
        runCmd(cmd)


def getWorkingBranch():
    stat = runCmd("git status -bs")
    branch = stat[3:stat.find('..')]   #  "## branchname"  or "## branchname...remote [ahead 1]     and branch can't have '..'
    return branch

################################

def deleteLocalBranch( branch ):
    runCmd("git branch -D " + branch)   # TODO: should a forced delete require another flag?

def doCleanup( branches ):
    for b in branches:
        if myGit.hasBranch(b):
            deleteLocalBranch(b)
        else:
            print "* no action needed on '"+b+"'"


def doFetch( vendor, cfg ):
    repoURI = cfg.get('_url')
    remote = myGit.getRemoteByURI( repoURI )
    if remote is None:
        myGit.addRemote( vendor, repoURI )

    vendorSubdir = False if cfg.get('_subdir') == "None" else cfg.get('_subdir')
    streeName = getSubtreeBranchName(repoURI)
    trackingBranch = getVendorBranchName(repoURI) if vendorSubdir else streeName
    if myGit.hasBranch(trackingBranch): #just fetch
        runCmd("git fetch "+ remote)
    else: # get remote branch
        runCmd("git checkout -b "+ trackingBranch +" "+ vendor +"/"+ cfg.get('_branch') )

    if vendorSubdir:
        runCmd("git checkout "+ trackingBranch)
        # get filtered
        if myGit.hasBranch(streeName):
            deleteLocalBranch(streeName)
        runCmd("git subtree split --prefix="+ vendorSubdir +" -b "+ streeName)

def _doFetch( vendor, cfg ):
    wb = getWorkingBranch()
    doFetch( vendor, cfg )
    if wb != getWorkingBranch():    # if we ended up on another branch during fetch, switch back
        runCmd("git checkout "+ wb)


def doImplant( vendor, cfg ):
    repoURI = cfg.get('_url')
    streeName = getSubtreeBranchName(repoURI)
    destDir = cfg.get('path')

    if not myGit.hasBranch(streeName):
        raise Exception("branch '"+ streeName +"' not found to implant! run --fetch first")
    if os.path.exists( destDir ):
        raise Exception("path already exists at " + destDir +"! use --update to update a subtree")

    displayPath = vendor +":/"+ cfg.get('_subdir') if cfg.get('_subdir') != 'None' else vendor

    runCmd('git subtree add --prefix='+ destDir +' --squash --message="import vendor code ('+displayPath+') into subdirectory ('+destDir+') as subtree" '+ streeName)


def doUpdate( vendor, cfg ):
    destDir = cfg.get('path')
    if not os.path.exists( destDir ):
        raise Exception("subtree does not exist at " + destDir +"!")

    _doFetch( vendor, cfg )

    repoURI = cfg.get('_url')
    streeName = getSubtreeBranchName(repoURI)
    result = runCmd('git subtree merge --prefix='+ destDir +' --squash '+ streeName)
    print "\n\n"+ result


def doContribute( vendor, cfg ):
    if cfg.get('_subdir') == 'None':
        _doFetch( vendor, cfg )
        result = runCmd("git subtree push --prefix="+cfg.get('path')+" "+cfg.get('url')+" "+cfg.get('branch'))
        print "\n\n"+ result
    else:
        raise Exception('--contribute not implemented yet for partial repo')


################################

if __name__ == "__main__":
    #do work

    argp = optparse.OptionParser(usage="\n%prog --<fetch|implant|update|contribute|cleanup> --vendor=<id>\nor\n%prog --<define> --vendor=<id> --vendorRepo=<url> [--vendorBranch=<name>] [--vendorDir=<path>] [--destDir=<path>]")
    ##              '--help' is a built-in option
    opts_act = optparse.OptionGroup( argp, 'Actions', 'action to perform (select one)')
    opts_act.add_option('--define', action="store_true", default=False, help="define a subtree")
    opts_act.add_option('--fetch', action="store_true", default=False, help="ensure there is an up-to-date copy of vendor code available")
    opts_act.add_option('--implant', action="store_true", default=False, help="import vendor code as subtree ('import' was a reserved word...)")
    opts_act.add_option('--update', action="store_true", default=False, help="'fetch' updated vendor code into subtree")
    opts_act.add_option('--contribute', action="store_true", default=False, help="stage changes made in your subtree for contribution back to vendor")
    opts_act.add_option('--cleanup', action="store_true", default=False, help="cleanup your local repo by removing vendor/subtree branches")
    argp.add_option_group(opts_act)
    argp.add_option('--vendor', action="store", help="identifier of vendor git repository")
    argp.add_option('--vendorDir', action="store", help="subdirectory of interest in vendor repository")
    argp.add_option('--destDir', action="store", help="subdirectory in project for vendor code")
    argp.add_option('--vendorRepo', action="store", help="vendor git repository url")
    argp.add_option('--vendorBranch', action="store", default="master", help="branch of vendor repository to use")
    argp.add_option('--wc', action="store", default=".", help="path to working copy")
    argp.add_option('--vendorGuessFromSubtree', action="store", help=optparse.SUPPRESS_HELP) #help="a file path in repository to guess the vendor from")

    options, args = argp.parse_args()
    opts = vars(options) # turn options into dict from "Values"

    #base command given
    actions = [a.dest for a in opts_act.option_list if opts.get(a.dest)]
    if len(actions) != 1:
        argp.error("--define, --fetch, --implant, --update, --contribute, and --cleanup are mutually exclusive")

    vendor = options.vendor
    if vendor is None and options.vendorGuessFromSubtree is None:
        argp.error("--vendor is required")

    if options.define:
        if options.vendorRepo is None:
            argp.error("--vendorRepo is required with --define")

    print "\ngit Subtree --"+ actions[0]
    start = time.time()

    wb = getWorkingBranch()
    if wb.startswith('vendor') or wb.startswith('subtree'):
        raise Exception('checked out branch does not appear to be a working branch!  please checkout a working branch before running commands')

    repoDir = opts.get('wc')

    #load .gittrees     #http://ruleant.blogspot.com/2013/06/git-subtree-module-with-gittrees-config.html
    trees = TreeConfig( repoDir +'/.gittrees' )

    if vendor is None:
        vendor = trees.guessVendorFromPath( options.vendorGuessFromSubtree )
        print "\nGuessed vendor is: "+ str(vendor)
        if vendor is None:
            print "\n* no action performed - path not in known subtree!\n"
            sys.exit()

    vendorExists = trees.vendorDefined( vendor )
    if options.define:
        if vendorExists:
            raise Exception('cannot define "{0}"; it already exists'.format(vendor))
        etc = {
            'destDir': opts.get('destDir'),
            'vendorDir': opts.get('vendorDir'),
            'vendorBranch': options.vendorBranch #default is master
        }
        trees.defineSubtree( vendor, options.vendorRepo, **etc )
    elif not vendorExists:
        raise Exception('"%s" has not been defined.  Use --define to add it'.format(vendor))
    else:
        subtree = trees.getSubtreeCfg(vendor)
        print "\nsubtree configuration for vendor '"+vendor+"':"
        pprint.pprint(subtree)
        print "\n"

        myGit = GitConfig() #global myGit

        if options.cleanup:
            branches = [subtree.get('branch')]
            if subtree.get('_subdir') != "None":
                branches.append( getVendorBranchName(subtree.get('_url')) )
            doCleanup(branches)

        elif options.fetch:
            doFetch( vendor, subtree )
        elif options.implant:
            doImplant( vendor, subtree )
        elif options.update:
            doUpdate( vendor, subtree )
        elif options.contribute:
            doContribute( vendor, subtree )


    end = time.time()
    elapsed = end - start
    print "\r\n%.2fs\r\n" % elapsed
