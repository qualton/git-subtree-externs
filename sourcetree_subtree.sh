#!/bin/bash

# using python script directly from SourceTree wouldn't work, so passing it through bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )   # get directory of this script
python $DIR/git_subtree_externs.py "$@"